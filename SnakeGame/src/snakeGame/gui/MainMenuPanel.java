package snakeGame.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenuPanel extends GuiPanel{
	private Font titleFont = new Font("TimesRoman", Font.PLAIN, 18);
	private Font creatorFont = new Font("TimesRoman", Font.PLAIN, 12);
	
	private String title = "Snake Game";
	private String creator = "By Daniel";
	private int buttonWidth = 220;
	private int buttonHeight = 60;
	
	private int spacing = 90;
	
	public MainMenuPanel() {
		super();
		GuiButton playButton = new GuiButton(300 / 2 - buttonWidth / 2, 220, buttonWidth, buttonHeight);
		GuiButton kittyButton = new GuiButton(300 / 2 - buttonWidth / 2, playButton.getY() + spacing, buttonWidth, buttonHeight);
		GuiButton starButton = new GuiButton(300 / 2 - buttonWidth / 2, kittyButton.getY() + spacing, buttonWidth, buttonHeight);
		
		playButton.setText("Common Snake");
		kittyButton.setText("Kitty (coming soon)");
		starButton.setText("Star (coming soon)");
		
		playButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiScreen.getInstance().setCurrentPanel("Common Snake");
				
			}
		});
		
		kittyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiScreen.getInstance().setCurrentPanel("Kitty (coming soon)");
				
			}
		});
		starButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiScreen.getInstance().setCurrentPanel("Star (coming soon)");
				
			}
		});
		
		add(playButton);
		add(kittyButton);
		add(starButton);
			
	}

	@Override
	public void render(Graphics2D g) {
		super.render(g);
		g.setFont(titleFont);
		g.setColor(Color.BLACK);
		g.drawString(title, 150, 150);
		
		g.setFont(creatorFont);
		g.setColor(Color.BLACK);
		g.drawString(creator, 20, 290);
	}
	
	
	
}
